<?php
    session_start();

    if((isset($_SESSION['zalogowany'])) && ($_SESSION['zalogowany'] == true)) {
        header('Location: gra.php');
        exit();
    }

    
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />

<title>Osadnicy - gra przeglądarkowa</title>

<link rel="stylesheet" href="style.css" />
<link href="https://fonts.googleapis.com/css?family=Archivo+Narrow" rel="stylesheet">
</head> 
<body>

<div class="form">
     <h3>Tylko martwi ujrzeli koniec wojny </h3> <br>
  
     <div id="title">Osadnicy</div>

     <form action="zaloguj.php" method="post">
        Login: <br> 
        <input type="text" name="login" /> <br>

        Hasło: <br>
        <input type="password" name="haslo" /> <br><br>
   
        <input id="submit" type="submit" value="Zaloguj się" />
      </form>

      <?php
         if(isset($_SESSION['blad'])){
             echo $_SESSION['blad'];
             unset($_SESSION['blad']);
         }
      ?>      
</div>
</body>
<html>    